public class TemperatureRange extends SafeRange {

	public TemperatureRange(double lowerBound, double upperBound) {
		super(lowerBound, upperBound);
	}
}
