import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class FactorMonitor {
	private final List<Patient> patients;
	private final Map<Patient, Map<Sensor, Map<Integer, Double>>> database;

	public FactorMonitor() {
		this.patients = new ArrayList<Patient>();
		this.database = new LinkedHashMap<Patient, Map<Sensor, Map<Integer, Double>>>();
	}

	public void addPatient(Patient patient) {
		this.patients.add(patient);
	}

	public void monitor(int monitorPeriod) throws IOException {
		for (int time = 0; time <= monitorPeriod; time++) {
			for (Patient patient : patients) {
				if (time % patient.getFrequency() == 0) {
					for (Sensor sensor : patient.getSensors()) {
						double factor = -1;
						try {
							factor = sensor.read();
							SafeRange safeRange = patient.getSafeRange(sensor);
							if (safeRange.isExceed(factor)) {
								displayAlarm(String.format("[%d] %s is in danger! Cause: %s %.1f", time,
										patient.getPatientName(), sensor.getSensorName(), factor));
							}
						} catch (SensorFailedException e) {
							displayAlarm(String.format("[%d] %s falls", time, sensor.getSensorName()));
						}
						saveToDatabase(patient, sensor, time, factor);
					}
				}
			}
		}
	}

	private void saveToDatabase(Patient patient, Sensor sensor, int time, double factor) {
		Map<Sensor, Map<Integer, Double>> sensors = database.get(patient);
		if (sensors == null) {
			sensors = new LinkedHashMap<Sensor, Map<Integer, Double>>();
			database.put(patient, sensors);
		}
		Map<Integer, Double> factors = sensors.get(sensor);
		if (factors == null) {
			factors = new LinkedHashMap<Integer, Double>();
			sensors.put(sensor, factors);
		}
		factors.put(time, factor);
	}

	public void displayDatabase() {
		for (Entry<Patient, Map<Sensor, Map<Integer, Double>>> patient : database.entrySet()) {
			System.out.println(patient.getKey().toString());
			for (Entry<Sensor, Map<Integer, Double>> sensor : patient.getValue().entrySet()) {
				System.out.println(sensor.getKey().toString());
				for (Entry<Integer, Double> factor : sensor.getValue().entrySet()) {
					System.out.printf("[%d] %.1f%n", factor.getKey(), factor.getValue());
				}
			}
		}
	}

	public void displayAlarm(String message) {
		System.out.println(message);
	}
}
