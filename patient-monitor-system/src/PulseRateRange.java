public class PulseRateRange extends SafeRange {

	public PulseRateRange(double lowerBound, double upperBound) {
		super(lowerBound, upperBound);
	}
}
