import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Patient {

	protected final String patientName;
	protected final int frequency;
	private final List<Sensor> sensors;
	private final Map<Sensor, SafeRange> safety;

	public Patient(String patientName, int frequency) {
		this.patientName = patientName;
		this.frequency = frequency;
		sensors = new ArrayList<Sensor>();
		safety = new HashMap<Sensor, SafeRange>();
	}

	public String getPatientName() {
		return patientName;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}

	public int getFrequency() {
		return frequency;
	}

	public SafeRange getSafeRange(Sensor sensor) {
		return safety.get(sensor);
	}

	public void attachSensor(Sensor sensor, SafeRange safeRange) {
		sensors.add(sensor);
		safety.put(sensor, safeRange);
	}

	@Override
	public int hashCode() {
		return patientName.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return other != null && other instanceof Patient && patientName.equals(((Patient) other).patientName);
	}

	@Override
	public String toString() {
		return String.format("patient %s", getPatientName());
	}
}
