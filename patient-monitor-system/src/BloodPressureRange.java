public class BloodPressureRange extends SafeRange {

	public BloodPressureRange(double lowerBound, double upperBound) {
		super(lowerBound, upperBound);
	}
}
