import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	private final int monitorPeriod;
	private final FactorMonitor factorMonitor;

	public Main(File file) throws IOException {
		factorMonitor = new FactorMonitor();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			monitorPeriod = Integer.parseInt(reader.readLine());
			Patient patient = null;
			for (String line; (line = reader.readLine()) != null;) {
				String[] args = line.split(" ");
				String category = args[0];
				if (category.equals("patient")) {
					String patientName = args[1];
					int frequency = Integer.parseInt(args[2]);
					patient = new Patient(patientName, frequency);
					factorMonitor.addPatient(patient);
				} else { // others should be sensors
					String sensorName = args[1];
					String fileName = args[2];
					double lowerBound = Double.parseDouble(args[3]);
					double upperBound = Double.parseDouble(args[4]);
					if (category.equals("TemperatureSensor")) {
						TemperatureSensor sensor = new TemperatureSensor(sensorName);
						sensor.setDataSource(loadFactors(fileName).iterator());
						TemperatureRange safeRange = new TemperatureRange(lowerBound, upperBound);
						patient.attachSensor(sensor, safeRange);
					} else if (category.equals("BloodPressureSensor")) {
						BloodPressureSensor sensor = new BloodPressureSensor(sensorName);
						sensor.setDataSource(loadFactors(fileName).iterator());
						BloodPressureRange safeRange = new BloodPressureRange(lowerBound, upperBound);
						patient.attachSensor(sensor, safeRange);
					} else if (category.equals("PulseSensor")) {
						PulseSensor sensor = new PulseSensor(sensorName);
						sensor.setDataSource(loadFactors(fileName).iterator());
						PulseRateRange safeRange = new PulseRateRange(lowerBound, upperBound);
						patient.attachSensor(sensor, safeRange);
					}
				}
			}
		}
	}

	static List<Double> loadFactors(String fileName) throws FileNotFoundException, IOException {
		List<Double> factors = new ArrayList<Double>();
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)))) {
			for (String line; (line = reader.readLine()) != null;) {
				double factor = Double.parseDouble(line);
				factors.add(factor);
			}
		}
		return factors;
	}

	public void solve() throws IOException {
		factorMonitor.monitor(monitorPeriod);
		factorMonitor.displayDatabase();
	}

	public static void main(String[] args) throws IOException {
		new Main(new File(args[0])).solve();
	}

}
