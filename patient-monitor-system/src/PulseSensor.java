import java.io.IOException;
import java.util.Iterator;

public class PulseSensor extends Sensor {
	private Iterator<Double> factors;

	public PulseSensor(String sensorName) {
		super(sensorName);
	}

	public void setDataSource(Iterator<Double> factors) {
		this.factors = factors;
	}

	@Override
	public double read() throws IOException, SensorFailedException {
		Double factor = factors.hasNext() ? factors.next() : -1.0;
		if (factor == -1) {
			throw new SensorFailedException();
		}
		return factor;
	}

	@Override
	public String toString() {
		return String.format("PulseSensor %s", getSensorName());
	}
}
