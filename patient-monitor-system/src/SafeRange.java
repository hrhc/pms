public abstract class SafeRange {
	private final double lowerBound;
	private final double upperBound;

	public SafeRange(double lowerBound, double upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public boolean isExceed(double factor) {
		return factor < lowerBound || factor > upperBound;
	}
}
