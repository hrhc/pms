import java.io.IOException;

public abstract class Sensor {
	private final String sensorName;

	public Sensor(String sensorName) {
		this.sensorName = sensorName;
	}

	public String getSensorName() {
		return sensorName;
	}

	public abstract double read() throws IOException, SensorFailedException;

	@Override
	public int hashCode() {
		return sensorName.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return other != null && other instanceof Sensor && sensorName.equals(((Sensor) other).sensorName);
	}
}
